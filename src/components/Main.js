require('normalize.css/normalize.css');
require('styles/App.css');


import React from 'react';
import Header from './header/header';
import Body from './body/body';
import Footer from './footer/footer';


class AppComponent extends React.Component {
  render() {
    return (
      <div className="appContainer">
        <Header> </Header>
        <Body> </Body>
        <Footer> </Footer>
      </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
