import React from 'react';
import Retailer from '../retailer/retailer';
import Toys from '../toys/toys';
import ToysInMobile from '../toys/toys-mobile';
import RangerVideo from '../video/ranger-video';
require('styles/body.css');

class Body extends React.Component {
  render() {
    return (
      <div className="body">
      <div className="body-header" />
      <div className="body-content"> 
      <div className="showToysInMobileMode" ><ToysInMobile></ToysInMobile></div>     
      <Retailer></Retailer>
      <div className="showToysInDesktopMode" id="shop"><Toys></Toys></div>
      <div ><RangerVideo></RangerVideo></div>
      </div>
      </div>
    );
  }
}

Body.defaultProps = {
};

export default Body;