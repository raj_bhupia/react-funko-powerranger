import React from 'react';
import {Grid, Col, Row, Button, FormGroup, InputGroup, Glyphicon, FormControl, Clearfix, MenuItem} from 'react-bootstrap';

require('styles/header.css');

const  socialmedia = {twitter:"https://twitter.com/thepowerrangers?lang=en",
					  facebook: "https://www.facebook.com/powerrangers/",
					  instagram: "https://www.instagram.com/powerrangers/?hl=en",
					  youtube: "https://www.youtube.com/watch?v=k-l08eWREk8"}

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state ={toggleMenu:false}
		this.toggleMenu = this.toggleMenu.bind(this);
	}
	toggleMenu() {
		this.setState({toggleMenu: !this.state.toggleMenu})
	}

	getMenuItems() {
		const MenuItems = (
		  <Clearfix>
		    <ul className="dropdown-menu open">
		      <MenuItem href="#products" onSelect={this.toggleMenu}>PRODUCTS</MenuItem>
		      <MenuItem divider/>
		      <MenuItem href="#licenses" onSelect={this.toggleMenu}>LICENSES</MenuItem>
		      <MenuItem divider/>
		      <MenuItem href="#retailers" onSelect={this.toggleMenu}>RETAILERS</MenuItem>
		      <MenuItem divider/>
		      <MenuItem href="#shop" onSelect={this.toggleMenu}>SHOP</MenuItem>
		      <MenuItem divider/>
		      <MenuItem href="#blogs" onSelect={this.toggleMenu}>BLOGS</MenuItem>
		    </ul>
		  </Clearfix>
		);

		return MenuItems;
	}

	  render() {
	    return (
	      <header>
	       	<Grid fluid={true}>
	       		<Row>
	       			<Col lgHidden={true} sm={2} xs={2} mdHidden={true}>
					     <Button className="glyphicon-without-border" onClick={this.toggleMenu}><Glyphicon glyph="align-justify" /></Button>
					     {this.state.toggleMenu && this.getMenuItems()}
					     <Button className="glyphicon-without-border"><Glyphicon glyph="search" /></Button>
					</Col>
	       			<Col lg={3} md={3} smHidden={true} xsHidden={true}>
					     <FormGroup>
					      <InputGroup className="search-input">
					        <FormControl className="search-input-box" type="text" placeholder="SEARCH"/>
					        <InputGroup.Addon className="glyphicon-without-border">
					          <Glyphicon glyph="search" />
					        </InputGroup.Addon>
					      </InputGroup>
					    </FormGroup>
					</Col>
	       			<Col lg={6} sm={8} xs={8} md={6} >
	       				<div className="logoContainer">
	        				<div className="bottom-half-circle"></div>
	        			</div>
	       			</Col>
	       			<Col lg={3} sm={2} xs={2} md={3} >
		       			<div className="buttonContainer">
		       				<div className="centerAlign">
			       				<Button bsStyle="primary" target="_blank" href={socialmedia.twitter} className="round-button twitter"></Button>
			       				<Button bsStyle="info" target="_blank" href={socialmedia.facebook} className="round-button facebook"></Button>
			       				<Button bsStyle="danger" target="_blank" href={socialmedia.instagram} className="round-button instagram"></Button>
			       				<Button bsStyle="danger" target="_blank" href={socialmedia.youtube} className="round-button youtube"></Button>
		       				</div>
		       			</div>
	       			</Col>
	       		</Row>
	       		<Row  >
	       			<Col lg={3} md={3} smHidden={true} xsHidden={true} className="hyperlink">
					      <Button  bsStyle="link" href="#products">PRODUCTS</Button>
					</Col>
					<Col lg={3} md={3} smHidden={true} xsHidden={true} className="hyperlink">
					     <Button  bsStyle="link" href="#licenses">LICENSES</Button>
					</Col>
	       			<Col lg={2} md={2} smHidden={true} xsHidden={true} className="hyperlink">
		       			 <Button  bsStyle="link" href="#retailers">RETAILERS</Button>
	       			</Col>
	       			<Col lg={2} md={2} smHidden={true} xsHidden={true} className="hyperlink">
		       			 <Button  bsStyle="link" href="#shop">SHOP</Button>
	       			</Col>
	       			<Col lg={2} md={2} smHidden={true} xsHidden={true} className="hyperlink">
	       				 <Button  bsStyle="link" href="#blog">BLOG</Button>
	       			</Col>
	       		</Row>
	       	</Grid>
	      </header>
	    );
	  }
}

Header.defaultProps = {
};

export default Header;