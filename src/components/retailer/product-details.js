import React from 'react';
import {Grid, Col, Row,Thumbnail} from 'react-bootstrap';
require('styles/product-details.css');

class ProductDetails extends React.Component {
  render() {
    return (  
      <div className="productDetails" id="products">   
        
		<Grid bsClass="productContainer">
		    <Row>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0004_Black-Ranger-in-Box.png" alt="242x200">
		        
		      </Thumbnail>
		    </Col>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0008_Pink-Ranger-Box.png" alt="242x200">
		      </Thumbnail>
		    </Col>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0002_Yellow-ranger-Box.png" alt="242x200">
		      </Thumbnail>
		    </Col>
		    </Row>
		    <Row>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0003_Blue-Ranger-Box.png" alt="242x200">
		        
		      </Thumbnail>
		    </Col>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0005_Red-Ranger-in-box.png" alt="242x200">
		      </Thumbnail>
		    </Col>
		    <Col xs={4}>
		      <Thumbnail src="../../images/PowerRangers_LP_v03_0003s_0001_Rita-in-the-Box.png" alt="242x200">
		      </Thumbnail>
		    </Col>
		    </Row>
		</Grid> 
      </div>
    );
  }
}

ProductDetails.defaultProps = {
};

export default ProductDetails;
