import React from 'react';
import {Image, Button} from 'react-bootstrap';
require('styles/retailer.css');

const retailesList = [{imageName:"amazon", href:"https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=power+rangers&rh=i%3Aaps%2Ck%3Apower+rangers"},
                      {imageName:"walgreens", href:"https://www.walgreens.com/search/results.jsp?Ntt=power+rangers"},
                      {imageName:"walmart", href:"https://www.walmart.com/search/?query=power%20rangers&cat_id=0"},
                      {imageName:"hottopic", href:"http://www.hottopic.com/pop-culture/shop-by-license/mighty-morphin-power-rangers/"},
                      {imageName:"toysrus", href:"http://www.toysrus.com/family/index.jsp?categoryId=11194092&sr=1&origkw=power%20rangers&ff=Taxonomy&fg=Category&fd=&fv=2254197"},
                      {imageName:"target", href:"https://www.target.com/s?searchTerm=Power%20Rangers"},
                      {imageName:"barnesnobles", href:"https://www.barnesandnoble.com/s/Power+Rangers?_requestid=428072"},
                      {imageName:"gamestop", href:"http://www.gamestop.com/browse?nav=16k-3-power+rangers,28zu0"}]
class RetailerView extends React.Component {
  render() {
    return (  
      <div className="retailer-view"id="retailers">
      <div className="retailerList">   
        {retailesList.sort((a,b) => a.imageName>b.imageName).map(retailer => {
          return (
             <div key={retailer.imageName} className="retailerContainer">
             <Button className="retailerButton" href={retailer.href} target="_blank"> <Image className="retailerImages" src={`../../images/${retailer.imageName}.png`}/> </Button>
              
             </div>
            )
        })} 
      </div>
      </div>
    );
  }
}

RetailerView.defaultProps = {
};

export default RetailerView;
