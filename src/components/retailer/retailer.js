import React from 'react';
import {Grid, Col, Row} from 'react-bootstrap';
import ProductDetails from "./product-details";
import RetailerView from "./retailer-view";
require('styles/retailer.css');

class Retailer extends React.Component {
  render() {
    return (  
      <div >   
        <div className="toy-message"> TOGETHER WE ARE MORE. VISIT A RETAILER TODAAY AND BRING THEM ALL</div>
        <div className="retailer-view">  
          <Grid fluid={true}>
            <Row>
              <Col md={6} lg={6} sm={12} xs={12}>
                <ProductDetails></ProductDetails>
              </Col>
              <Col md={6} lg={6} sm={12} xs={12}>
                <RetailerView></RetailerView>
              </Col>
            </Row>
          </Grid>
          </div>  
      </div>
    );
  }
}

Retailer.defaultProps = {
};

export default Retailer;
