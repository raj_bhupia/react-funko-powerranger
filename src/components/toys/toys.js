import React from 'react';
import {Grid, Col, Row, Image} from 'react-bootstrap';
require('styles/toys.css');

class Toys extends React.Component {
  render() {
    return (  
      <div className="toys">   
        
		<Grid className="toysGrid" >
		    <Row>
		    <Col xs={4}>
		    	<div className="toyContainer">
		      		<Image src= "../../images/rita-inclined.png" className="base"/>
			        <Image src= "../../images/mobile.png" className="mobile-overlay" style={{zIndex:1}}/>
			        <Image src= "../../images/red-power-ranger-mobile-view.png" className="mobile-screen-overlay" style={{zIndex:2}}/>
		        </div>
		    </Col>
		    <Col xs={8}>
		        <Grid className="toysGrid" >
				    <Row>
				    <Col>
				    	<div className="toy-message"> COMING TO A PHONE NEAR YOU.<br/> JUMP ON QUIDD AND GET THEM BEFORE THEY'RE GONE</div>
				    </Col>
				    </Row>
				    <Row>
				    <Col xs={3}>
				    	<Image src= "../../images/black-power-ranger-lg.png"/>
				    </Col>
				    <Col xs={3}>
				    	<Image src= "../../images/yellow-power-rangeer.png"/>
				    </Col>
				    <Col xs={3}>
				    	<Image src= "../../images/pink-power-ranger.png"/>
				    </Col>
				    <Col xs={3}>
				    	<Image src= "../../images/blue-power-ranger.png"/>
				    </Col>
				    </Row>
				</Grid> 
		    </Col>
		    
		    </Row>
		</Grid> 
      </div>
    );
  }
}

Toys.defaultProps = {
};

export default Toys;
